%!TEX program=xelatex
\documentclass[utf8,a4paper]{moderncv}
\usepackage[left=3.5cm,right=3.5cm]{geometry}
\usepackage{polyglossia}
\setdefaultlanguage[variant=british]{english}
\setotherlanguage{dutch}

\usepackage[style=apa,sorting=ydnt,dashed=false]{biblatex}
\addbibresource[label=cv]{cv.bib}
\addbibresource[label=media]{media.bib}
\addbibresource[label=speaking]{public-speaking.bib}
\addbibresource[label=cve]{cve.bib}

\usepackage[unicode]{hyperref}
\usepackage{cleveref}

\renewbibmacro*{date}{}
% \renewbibmacro*{date+extrayear}{}
\renewbibmacro*{issue+date}{}
\renewbibmacro*{url+urldate}{%
  \iftoggle{bbx:url}
    {\usebibmacro{url}}
    {}}
\newcommand*{\bibyear}{}

\defbibenvironment{bibliography}
  {\list
     {\iffieldequals{year}{\bibyear}
        {}
        {\printfield{year}%
         \savefield{year}{\bibyear}}}
     {\setlength{\topsep}{0pt}% layout parameters based on moderncvstyleclassic.sty
      \setlength{\labelwidth}{\hintscolumnwidth}%
      \setlength{\labelsep}{\separatorcolumnwidth}%
      \setlength{\itemsep}{\bibitemsep}%
      \leftmargin\labelwidth%
      \advance\leftmargin\labelsep}%
      \sloppy\clubpenalty4000\widowpenalty4000}
  {\endlist}
  {\item}

  % Set Ruben De Smet in bold
\edef\authorhash{\detokenize{3b7490637c600e6d9d2d83d7fd07826c}}

\renewcommand{\mkbibnamegiven}[1]{%
  \iffieldequals{hash}{\authorhash}{\mkbibbold{#1}}{#1}}
\renewcommand{\mkbibnamefamily}[1]{%
  \iffieldequals{hash}{\authorhash}{\mkbibbold{#1}}{#1}}
\renewcommand{\mkbibnameprefix}[1]{%
  \iffieldequals{hash}{\authorhash}{\mkbibbold{#1}}{#1}}
\renewcommand{\mkbibnamesuffix}[1]{%
  \iffieldequals{hash}{\authorhash}{\mkbibbold{#1}}{#1}}

\newcommand{\nth}[1]{#1\textsuperscript{th}}
\newcommand{\first}{1\textsuperscript{st}}

\usepackage{graphicx}
\usepackage{fontspec}
% \usepackage{mathpazo}
\setmainfont{TeX Gyre Pagella}

\moderncvcolor{blue}
\moderncvstyle{casual}

\firstname{Ruben}
\lastname{De Smet}

%\title{}
% \address{Eikenlaan 2}{2620 Hemiksem, Belgium}
% \mobile{+32 474 09 11 50}
\email{me@rubdos.be}
\social[github]{rubdos}
\social[gitlab]{rubdos}
\social[signal][signal.me/\#eu/snJWgAFbLBsBhexpsKuL2SpDuTgohfrfZqvz7C3oalAnZv2pAPdFxar-zJC-0bNE]{rubdos.95}
\social[linkedin]{rubdos}
\social[twitter]{@rubdos}
\homepage{www.rubdos.be}
\photo[48pt][0.2pt]{ruben.jpg}

\renewcommand\emph[1]{{\color{color1}#1}}

\begin{document}
\makecvtitle%

\setlength{\hintscolumnwidth}{2.5cm}

\section{About me}

  A privacy advocate since high school,
  with extensive experience in applied \emph{cryptography} and \emph{privacy}-enhancing technologies in \emph{Rust}.
  Passionate about developing decentralized real-world solutions that protect user privacy.
  This passion reflects in projects like \emph{Whisperfish} and Glycos.

\section{Education}
\cventry{2018--2024}{Doctor of Philosophy}{\textdutch{Vrije Universiteit Brussel}}{}{}{
  Research on zero-knowledge proofs as privacy-enhancing technology.
}
\cventry{2016--2018}{Master of Science}{\textdutch{Vrije Universiteit Brussel}}{}{}{Engineering Science: Computer Science: Multimedia}
\cventry{2013--2017}{Bachelor of Science}{\textdutch{Vrije Universiteit Brussel}}{}{}{Engineering Science: Electronics and Information Technology, computer science}

\section{Skills}

\newcommand\skillsep{{\color{color1}\hrule}\vspace{.25em}}

\cvdoubleitem{Programming}{\cvskill{5} \quad Rust}{}{\cvskill{4} \quad C}
\cvdoubleitem{}{\cvskill{4} \quad Python}{}{\cvskill{3} \quad Java}

\skillsep

\cvdoubleitem{Languages}{\cvskill{5} \quad Dutch}{}{\cvskill{4} \quad English}
\cvitem{}{\cvskill{3} \quad French}

\section{Experience}
\cventry{2024--now}{Postdoctoral researcher applied cryptography}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}{}{%
  \begin{itemize}
    \item Research projects in \emph{cryptography} and \emph{privacy} engineering
    \item Technology projects regarding \emph{Rust}, cloud and IoT
  \end{itemize}
}

\cventry{2019--now}{Whisperfish}{lead developer}{}{Unofficial Signal app for Sailfish OS}{
  \begin{itemize}
    \item Rewritten in \emph{Rust}, fully backwards compatible
    \item Implemented large parts of the Signal API
  \end{itemize}
}

\cventry{2018--2024}{Teaching assistant}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}{}{%
  \begin{itemize}
    \item Teaching programming and electronics
    \item Research projects in \emph{cryptography} and \emph{privacy} engineering
  \end{itemize}
}

% Start of detailed section
\cleardoublepage

\raggedleft
\titlestyle{Detailed CV}

\section{Education}

\cventry{2018--2024}{Doctor of Philosophy}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}%
{\textit{Engineering Science: PhD Electronics and Information Technology}}%
{
applied cryptography, security, privacy, telecommunication.
}

\cventry{2016--2018}{Master of Science}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}%
{\textit{Engineering Science: Computer Science: Multimedia}}%
{
computer vision, image and video technology, digital speech and audio processing, cryptography and security.%
}

\cventry{2013--2017}{Bachelor of Science}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}{\textit{Engineering Science: Electronics and Information Technology, computer science}}{basic electronics, computer networks and protocols, algorithms, data structures, computer architectures}

\cventry{2006--2013}{ASO}{Sint-Ritacollege}{Kontich, Belgium}{\textit{\textdutch{Wetenschappen-Wiskunde}}}{}

\section{Experience \small{references on request}}
\cventry{2024--now}{Postdoctoral researcher applied cryptography}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}{}{%
  I am involved in several research and technology projects regarding Rust, IoT, privacy and security.
  My research mainly involves development of practical privacy-enhancing technologies.
  For details, see the academic section at~\cpageref{sec:academic-projects}.
}
\cventry{2018--2024}{Teaching assistant}{\textdutch{Vrije Universiteit Brussel}}{Brussels, Belgium}{}{%
  As a teaching assistant, I was co-responsible for the first bachelor Informatica (``programming 101'') course and a part of the third bachelor electronics course at the Engineering Sciences faculty.
  I was also involved in several research and technology projects, regarding Rust and cloud technologies.
  For details, see the academic section at~\cpageref{sec:academic-projects}.
}

\cventry{2015--now}{Owner at De Smet Multimedia BV}{Consulting}{Brussels / Antwerp, Belgium}{}{General IT, implementation and integration.}

\subsection{Freelance}

\cventry{2016--now}{Tutoring secondary education}{}{}{}{Long term tutoring mathematics and physics for \nth{5}~and \nth{6}~secondary education. Tutoring computer science topics for \first~Bachelor.}

\cventry{2015--2018}{Freelance Linux system administrator}{BruWind/Vrije Universiteit Brussel}{Brussels, Belgium}{}{Deploying, testing, administrating a CentOS~7 cluster using Ansible.
System monitoring.
Continuous integration, complex continuous deployment of Docker based micro service architecture.}

\subsection{Student work}
\cventry{July 2015}{ETL implementation}{Vrije Universiteit Brussel}{Brussels, Belgium}{}{Implemented ETL software using Python\,3 and SOS-Berlin JobScheduler. Loading vast amounts of windturbine sensor data into a PostgreSQL 9.4 database. \\
Worked on \href{https://github.com/adamreeve/npTDMS}{npTDMS} and \href{https://github.com/rubdos/TDMSpp}{TDMSpp}, of which the latter improves the performance of the former with a factor 5}

\cventry{2010--2014}{IT specialist}{NetLogic}{Duffel, Belgium}{}{General IT and server administration for a small company}

\section{Personal projects}
\cventry{2023--now}{blurhash-rs}{maintainer}{}{}{
  Together with the Whisperfish team, I took over the maintenance of this widely used blurhash computing library.
  I implemented multiple performance improvements,
  which makes this library by far the fastest blurhash decoding and encoding library available.
  \url{https://github.com/whisperfish/blurhash-rs}
}

\cventry{2023--now}{rust-phonenumber}{maintainer}{}{}{
  Together with the Whisperfish team, I took over the maintenance of this widely used phonenumber parsing library.
  \url{https://github.com/whisperfish/rust-phonenumber}
}

\cventry{2019--now}{Whisperfish}{lead developer}{}{}{
  Whisperfish is a third-party implementation of the Signal protocol stack for Sailfish OS.
  It was left unmaintained in 2019, and I took over development.
  Whisperfish is rapidly growing in users, and has gotten the attention of several other communities.
  Whisperfish is becoming the go-to community for third-party Signal client development.
  \url{https://gitlab.com/whisperfish}
}

\cventry{2019--now}{Belgium Rust user group Organiser}{}{}{}{
  I have become the official ``Organiser'' of the Belgium Rust user group, since 2019.
  Due to the COVID-19 pandemic, we only started organising physical events again in February 2022.
  We organise events for Rust enthusiasts and engineers all around Belgium, at different locations, typically centrally reachable in Brussels.
}
\cventry{2017--now}{Belgium Rust user group member}{}{}{}{
  Having spoken about a plethora of topics at all Meetups until now, I have been an active member of the Belgium Rust user group until the start of the pandemic.
}

\cventry{2016--now}{Official \LaTeX\ assets for the Vrije Universiteit Brussel}{}{}{}{Both \texttt{beamer} and \texttt{article} class templates for the new VUB style, compatible with most \LaTeX\ distributions. \url{https://gitlab.com/rubdos/texlive-vub}}

\cventry{2014--2019}{TheBounty Renderer}{}{}{}{A free software fork of the famous \textit{YafaRay} raytracer, mostly working on documentation using \LaTeX\ and GNU Make and optimising the Blender Exporter by porting Python\,3 code to C++. I also maintain its custom (Ruby on Rails) website and forum.}

\cventry{2013--now}{Glycos}{}{}{}{%
Development of a private, performant, and extensible peer-to-peer online social network.
Starting as a personal project in high school, Glycos evolved into my current academic research; see \cpageref{entry:academic:glycos}.
}


\section{Languages}

\cvlanguage{Dutch}{Native}{}

\cvlanguage{English}{Proficient}{}

\cvlanguage{French}{Conversational}{}

\section{Engineering skills}

\cvitem{System}{Kubernetes, Docker, nginx, Ansible, PostgreSQL}

\cvitem{CI/CD}{GitLab CI, Github Actions}

\cvitem{Development}{Rust, C, Bash/Shell scripting, Python}

\cvitem{Cryptography}{Elliptic curve cryptography, zero-knowledge protocols, Signal protocol stack}

\cvitem{Misc.}{Git, \LaTeX, vim, cross compiling}

% \subsection{Linux distributions}
%
% \cvitem{Red Hat family}{Most of the time, my workstations consist of \textbf{Fedora}; I worked with Fedora as of version 17.}
%
% \cvitem{Debian family}{I tend to give family and friends Ubuntu (I worked with versions 9.10 to 22.04, including on a collection of Odroids and other SBCs). I also used Debian on PowerPC G4, Raspberry PI.}
%
% %\cvitem{OpenEmbed}{I implemented a Yocto based embedded system for high throughput sensor readouts.}
%
% \cvitem{Others}{I tend to like Arch Linux on my most used personal systems, although I revert to the Red Hat family for professional use.}

% \section{Certifications}
%
% \cvitem{LPI}{LPIC-1 Februari 2014 \textit{(verification on request)}}

\section{Hobbies and Interests}

\cvitem{Chess}{%
  I thought chess to the youth of the Schaakkring Oude God in Mortsel.
  I participate in the national interclub competition.
}

\cvitem{Free software \& Privacy}{
  I am a proponent of the free software philosophy,
  and a privacy advocate.
  I believe free software and digital privacy go hand-in-hand.
}

\cvitem{Home Automation}{I have an extensive home automation and domotics system based on Home Assistant and other free software.}

% Start academic part
\clearpage

\raggedleft
\titlestyle{Academic CV}

\section{Doctoral thesis}
\label{academic}
\label{sec:doctoral-thesis}
% TODO: some details/key findings would be nice
\cventry{June 17, 2024}{Rapid prototyping and deployment of privacy-enhancing technologies}{Vrije Universiteit Brussel}{Brussels, Belgium}{Engineering Science}{}
\cvline{promoters}{prof.\ dr.\ ir.\ Kris Steenhaut and prof.\ dr.\ An Braeken}

\section{Master thesis}
% TODO: some details/key findings would be nice
\cvline{Glycos}{an extensible, resilient and private peer-to-peer online social network}
\cvline{}{development and implementation of an abstract tool set for building performant decentralised online social networks.}

\cvline{promoters}{prof.\ dr.\ Ann Dooms and prof.\ dr.\ Jo Pierson}

\section{Research and technology projects}
\label{sec:academic-projects}
% TODO: add CIF/Innoviris?
\cventry{2022--2024}{RustIEC}{Vrije Universiteit Brussel}{Brussels, Belgium}{}{%
% TODO: clarify my own role in this project
  Under the promotorship of prof.\ Kris Steenhaut, prof.\ An Braeken, dr.\ Jorn Lapon and prof.\ Stijn Volckaert, I am co-leading the RustIEC project.
  RustIEC is a VLAIO TETRA project (grant HBC.2021.0066) with the goal of teaching Flanders' companies proficiency in the Rust programming language. The project has a specific focus on secure IoT systems, secure edge computing, and secure cloud computing.
  -- \url{https://rustiec.be}
}

\cventry{2021--now}{ETRO JupyterLab}{Vrije Universiteit Brussel}{Brussels, Belgium}{}{%
  JupyterLab is a platform that hosts Jupyter Notebooks.
  The goal of this setup is to provide all engineering students at the faculty an online environment to learn Python.
  This platform has been online since 2021,
  \href{integrated with the university's Canvas LMS}{https://gitlab.com/etrovub/smartnets/jupyterhub/canvas-sync/},
  and is used in seven courses ranging from first Bachelor Informatica (``programming 101'')
  to ``vibrations and acoustics''.\\
  The system is setup on our internal Kubernetes cluster,
  and integrates \href{nbgrader}{https://nbgrader.readthedocs.io/en/stable/},
  for use in exercise sessions and during exams. \\
  I am the main responsible for its rollout, maintenance and daily operation.
}

\cventry{2019--2023}{OpenCloudEdge}{Vrije Universiteit Brussel}{Brussels, Belgium}{}{%
  This technology-transfer project (VLAIO TETRA) had the goal of teaching companies in Flanders to work with cloud technologies,
  among which OpenStack and Kubernetes.
  I was active in the Kubernetes tests.
}

\cventry{2018--now}{Glycos}{}{}{}{%
\label{entry:academic:glycos}
Research on private, performant, and extensible peer-to-peer online social networks.
Heavy use of modern cryptography (elliptic curve signatures, zero-knowledge systems). \\
Prof.\ dr.\ Ann Dooms made it possible to work on this on an academic level for my master thesis together with digital privacy expert prof.\ dr.\ Jo Pierson,
which was then continued in my PhD study under supervision of prof.\ dr. An Braeken and prof.\ dr.\ ir.\ Kris Steenhaut. \\
Glycos has now become the topic of my postdoctoral research,
and subtopics are part of the doctoral research of multiple colleagues.
}


% Publications
\newrefsection[cv]
\nocite{*}
\printbibliography[title={Publications}]

% Media contributions
\newrefsection[media]
\nocite{*}
\printbibliography[title={Appearance in media}]

% Media contributions
\newrefsection[speaking]
\nocite{*}
\printbibliography[title={Public speaking}]

% Common vulnerability enumerations
\newrefsection[cve]
\nocite{*}
\printbibliography[title={CVE's}]

\end{document}
