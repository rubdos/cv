PDF=cv.pdf cv.nl.pdf
BIB=cv.bib cve.bib media.bib public-speaking.bib

all: $(PDF)

# .PHONY: cv.pdf

%.pdf: %.tex ruben.jpg $(BIB)
	latexmk -use-make $<

cv.%.tex: cv.%.po cv.tex
	po4a-translate -f latex -m cv.tex -p $< -l $@
	# po4a --no-update --master cv.tex --po $< -l $@

cv.%.po: cv.tex
	po4a-updatepo -f latex -m $< -p $@
	# po4a --no-translations --master $< --po $@

clean:
	latexmk -CA cv
	latexmk -CA cv.nl
